﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AzureBlobStorageAPI.Models;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.IdentityModel.Protocols;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using System.Web.Helpers;

namespace AzureBlobStorageAPI.Controllers
{
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly CloudBlobContainer blobContainer;

        public ImageController()
        {
            var storageConnectionString = "https://highvelocitystorage.blob.core.windows.net";

            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);

            // We are going to use Blob Storage, so we need a blob client.
            var blobClient = storageAccount.CreateCloudBlobClient();

            // Data in blobs are organized in containers.
            // Here, we create a new, empty container.
            blobContainer = blobClient.GetContainerReference("employee-app");
            blobContainer.CreateIfNotExistsAsync();

            // We also set the permissions to "Public", so anyone will be able to access the file.
            // By default, containers are created with private permissions only.
            blobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
        }

        [HttpPost, DisableRequestSizeLimit]
        public async void PostAsync()
        {
            var image = WebImage.GetImageFromRequest();
            var imageBytes = image.GetBytes();

            // The parameter to the GetBlockBlobReference method will be the name
            // of the image (the blob) as it appears on the storage server.
            // You can name it anything you like; in this example, I am just using
            // the actual filename of the uploaded image.
            var blockBlob = blobContainer.GetBlockBlobReference(image.FileName);
            blockBlob.Properties.ContentType = "image/" + image.ImageFormat;

            await blockBlob.UploadFromByteArrayAsync(imageBytes, 0, imageBytes.Length);
        }
    }
}
